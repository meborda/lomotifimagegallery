package com.lomotif.lomotifimagegallery.repository

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.DataSource
import com.lomotif.lomotifimagegallery.models.Hit

class HitsDataFactory() :
    DataSource.Factory<Int, Hit>() {

    val mutableLiveData: MutableLiveData<RestRepository>
    private lateinit var hitsDataSource: RestRepository

    init {
        this.mutableLiveData = MutableLiveData()
    }

    override fun create(): DataSource<Int, Hit> {
        hitsDataSource = RestRepository()
        mutableLiveData.postValue(hitsDataSource)
        return hitsDataSource
    }
}

package com.lomotif.lomotifimagegallery.repository

import android.annotation.SuppressLint
import android.arch.paging.PageKeyedDataSource
import com.lomotif.lomotifimagegallery.models.Data
import com.lomotif.lomotifimagegallery.models.Hit
import com.lomotif.lomotifimagegallery.network.ApiBuilder
import io.reactivex.Observable

@SuppressLint("CheckResult")
class RestRepository : PageKeyedDataSource<Int, Hit>() {

    fun retrieveHits(page: Int): Observable<Data> {
        return ApiBuilder.apiHandler.retrieveHits(page)
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Hit>) {
        // do nothing
    }

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, Hit>) {
        val currentPage = 1
        val nextPage = currentPage + 1
        retrieveHits(currentPage).subscribe { data ->
            callback.onResult(data.hits.toMutableList(), currentPage, nextPage)
        }
    }

    override fun loadAfter(
        params: LoadParams<Int>,
        callback: LoadCallback<Int, Hit>
    ) {

        val currentPage = params.key
        val nextPage = currentPage + 1

        retrieveHits(currentPage).subscribe { data ->
            callback.onResult(data.hits.toMutableList(), nextPage)
        }
    }
}
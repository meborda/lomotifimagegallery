package com.lomotif.lomotifimagegallery.network

import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory

object ApiBuilder {

    const val HOST_URL = "https://pixabay.com/"
    const val API_KEY = "10961674-bf47eb00b05f514cdd08f6e11"

    private var retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(HOST_URL)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .addConverterFactory(ScalarsConverterFactory.create())
        .build()

    var apiHandler: ApiRequests = retrofit.create(ApiRequests::class.java)
}
package com.lomotif.lomotifimagegallery.network

import com.lomotif.lomotifimagegallery.models.Data
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query


interface ApiRequests {
    @GET("api")
    fun retrieveHits(
        @Query("page") page: Int,
        @Query("key") key: String? = ApiBuilder.API_KEY
    ): Observable<Data>
}
package player.lomotif.com.lomotifplayer.adapters

import android.arch.paging.PagedListAdapter
import android.content.Context
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.lomotif.lomotifimagegallery.R
import com.lomotif.lomotifimagegallery.models.Hit
import kotlinx.android.synthetic.main.item_hit.view.*

class HitsAdapter(
    private val context: Context,
    val onClick: View.OnClickListener
) :
    PagedListAdapter<Hit, HitsAdapter.HitViewHolder>(DIFF_CALLBACK) {

    override fun onBindViewHolder(holder: HitViewHolder, position: Int) {
        val hit = getItem(position)
        hit?.let {
            Glide.with(context)
                .load(it.previewURL)
                .into(holder.imageThumbnail)
            holder.itemView.tag = it
            holder.itemView.setOnClickListener(onClick)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HitViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_hit, parent, false)
        return HitViewHolder(view)
    }

    class HitViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val imageThumbnail: ImageView = itemView.imageThumbnail
    }

    companion object {
        private val DIFF_CALLBACK = object :
            DiffUtil.ItemCallback<Hit>() {
            // Concert details may have changed if reloaded from the database,
            // but ID is fixed.
            override fun areItemsTheSame(item1: Hit, item2: Hit): Boolean {
                return item1.id == item2.id
            }

            override fun areContentsTheSame(item1: Hit, item2: Hit): Boolean {
                return item1 == item2
            }
        }
    }
}

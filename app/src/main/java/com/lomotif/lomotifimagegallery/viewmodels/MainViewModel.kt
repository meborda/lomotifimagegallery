package com.lomotif.lomotifimagegallery.viewmodels

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import com.lomotif.lomotifimagegallery.models.Data
import com.lomotif.lomotifimagegallery.models.Hit
import com.lomotif.lomotifimagegallery.repository.HitsDataFactory
import java.util.concurrent.Executor
import java.util.concurrent.Executors

class MainViewModel() : ViewModel() {

    var executor: Executor = Executors.newFixedThreadPool(5)
    var pagedLiveData: LiveData<PagedList<Hit>>? = null

    val data: MutableLiveData<Data> = MutableLiveData()
    val hasError: MutableLiveData<Boolean> = MutableLiveData()


    init {
        val pagedListConfig = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPageSize(20)
            .build()

        pagedLiveData = LivePagedListBuilder(HitsDataFactory(), pagedListConfig)
            .setFetchExecutor(executor)
            .build()
    }

}
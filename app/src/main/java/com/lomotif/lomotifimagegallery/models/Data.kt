package com.lomotif.lomotifimagegallery.models

class Data(
    val totalHits: Int,
    val hits: ArrayList<Hit>,
    val total: Int
)
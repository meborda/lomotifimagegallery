package com.lomotif.lomotifimagegallery.models

import android.os.Parcel
import android.os.Parcelable

class Hit(
    val largeImageURL: String,
    val webformatHeight: Int,
    val webformatWidth: Int,
    val likes: Int,
    val imageWidth: Int,
    val id: Int,
    val user_id: Int,
    val views: Int,
    val comments: Int,
    val pageURL: String,
    val imageHeight: Int,
    val webformatURL: String,
    val type: String,
    val previewHeight: Int,
    val tags: String,
    val downloads: Int,
    val user: String,
    val favorites: Int,
    val imageSize: Int,
    val previewWidth: Int,
    val userImageURL: String,
    val previewURL: String
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(largeImageURL)
        parcel.writeInt(webformatHeight)
        parcel.writeInt(webformatWidth)
        parcel.writeInt(likes)
        parcel.writeInt(imageWidth)
        parcel.writeInt(id)
        parcel.writeInt(user_id)
        parcel.writeInt(views)
        parcel.writeInt(comments)
        parcel.writeString(pageURL)
        parcel.writeInt(imageHeight)
        parcel.writeString(webformatURL)
        parcel.writeString(type)
        parcel.writeInt(previewHeight)
        parcel.writeString(tags)
        parcel.writeInt(downloads)
        parcel.writeString(user)
        parcel.writeInt(favorites)
        parcel.writeInt(imageSize)
        parcel.writeInt(previewWidth)
        parcel.writeString(userImageURL)
        parcel.writeString(previewURL)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Hit?> {
        override fun createFromParcel(parcel: Parcel): Hit? {
            return Hit(parcel)
        }

        override fun newArray(size: Int): Array<Hit?> {
            return arrayOfNulls(size)
        }
    }
}
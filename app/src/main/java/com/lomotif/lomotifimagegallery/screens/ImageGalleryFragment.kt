package com.lomotif.lomotifimagegallery.screens

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.lomotif.lomotifimagegallery.R
import com.lomotif.lomotifimagegallery.models.Hit
import com.lomotif.lomotifimagegallery.viewmodels.MainViewModel
import kotlinx.android.synthetic.main.fragment_gallery.*
import player.lomotif.com.lomotifplayer.adapters.HitsAdapter

class ImageGalleryFragment : Fragment() {

    private lateinit var viewModel: MainViewModel
    private lateinit var adapter: HitsAdapter
    var navigateToPreiewCallback: NavigateToPreviewCallback? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_gallery, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)

        attachObservers()
        adapter = HitsAdapter(requireContext(), onClick)
        recycler.adapter = adapter
        recycler.layoutManager = StaggeredGridLayoutManager(
            2, StaggeredGridLayoutManager.VERTICAL
        )
    }

    val onClick = View.OnClickListener {
        it?.let {
            var tag = it.tag
            if (tag != null && tag is Hit && lifecycle.currentState.isAtLeast(Lifecycle.State.CREATED)) {
                navigateToPreiewCallback?.navigateToPreview(tag)
            }
        }
    }

    fun attachObservers() {
        viewModel.hasError.observe(this, Observer {
            it?.let { hasError ->
                if (hasError)
                    Toast.makeText(context, R.string.retrieve_error, Toast.LENGTH_SHORT).show()
            }
        })
        viewModel.pagedLiveData?.observe(this, Observer {
            it?.let {
                adapter.submitList(it)
            }
        })
    }


    interface NavigateToPreviewCallback {
        fun navigateToPreview(hit: Hit)
    }


}
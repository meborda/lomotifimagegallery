package com.lomotif.lomotifimagegallery.screens

import android.Manifest
import android.app.DownloadManager
import android.arch.lifecycle.Lifecycle
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.view.*
import android.widget.Toast
import com.bumptech.glide.Glide
import com.lomotif.lomotifimagegallery.R
import com.lomotif.lomotifimagegallery.models.Hit
import com.lomotif.lomotifimagegallery.utils.PermissionHandler
import kotlinx.android.synthetic.main.fragment_image_preview.*
import player.lomotif.com.lomotifplayer.extensions.isPermissionGranted
import player.lomotif.com.lomotifplayer.extensions.openAppSettings

class ImagePreviewFragment : Fragment(), PermissionHandler.PermissionResultCallback {

    private var hit: Hit? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        hit = arguments?.getParcelable<Hit>(EXTRA_IMAGE_HIT)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_image_preview, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        hit?.let { item ->
            Glide.with(this)
                .load(item.largeImageURL)
                .into(imageView)
        }
    }


    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_download, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_download -> {
                if (activity?.isPermissionGranted() == true && hit != null)
                    downloadImage(
                        hit!!.largeImageURL,
                        hit!!.largeImageURL.substring(hit!!.largeImageURL.lastIndexOf('/') + 1)
                    )
                else if (activity != null)
                    requestPermissions(
                        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        PERMISSION_WRITE_REQUEST
                    )
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == PERMISSION_WRITE_REQUEST)
            PermissionHandler.handlePermissionResult(requireActivity(), permissions, grantResults, this)
    }

    private fun downloadImage(url: String, filename: String) {
        val request = DownloadManager.Request(
            Uri.parse(url)
        )
        request.allowScanningByMediaScanner()
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, filename)
        val dm = context?.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        dm.enqueue(request)
        Toast.makeText(
            context, String.format(getString(R.string.downloading_file), filename),
            Toast.LENGTH_LONG
        ).show()
    }

    override fun onPermissionGranted() {
        hit?.let { item ->
            {
                downloadImage(
                    item.largeImageURL,
                    item.largeImageURL.substring(item.largeImageURL.lastIndexOf('/') + 1)
                )
            }
        }
    }

    override fun onPermissionDenied() {
        Toast.makeText(context, R.string.permission_denied, Toast.LENGTH_SHORT).show()
    }

    override fun onDeniedNeverAskTriggered() {
        showPermissionSnackbar()
    }

    private fun showPermissionSnackbar() {
        if (lifecycle.currentState.isAtLeast(Lifecycle.State.CREATED)) {
            val permissionSnackbar = Snackbar.make(
                container,
                R.string.permission_denied,
                Snackbar.LENGTH_INDEFINITE
            )
            permissionSnackbar.setAction(R.string.permission_grant) {
                requireContext().openAppSettings()
            }
            permissionSnackbar.show()
        }
    }

    companion object {
        const val EXTRA_IMAGE_HIT = "extra_hit_image"
        const val PERMISSION_WRITE_REQUEST = 1001

        fun newInstance(hit: Hit): ImagePreviewFragment {
            val fragment = ImagePreviewFragment()
            val bundle = Bundle()
            bundle.putParcelable(EXTRA_IMAGE_HIT, hit)
            fragment.arguments = bundle
            return fragment
        }
    }

}
package com.lomotif.lomotifimagegallery.screens

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.lomotif.lomotifimagegallery.R
import com.lomotif.lomotifimagegallery.models.Hit

class MainActivity : AppCompatActivity(),
    ImageGalleryFragment.NavigateToPreviewCallback {

    private var imageFragment: ImagePreviewFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val galleryFragment = ImageGalleryFragment()
        galleryFragment.navigateToPreiewCallback = this
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentContainer, galleryFragment)
            .commit()
    }

    override fun navigateToPreview(hit: Hit) {
        supportFragmentManager
            .beginTransaction()
            .add(
                R.id.fragmentContainer,
                ImagePreviewFragment.newInstance(hit)
            )
            .addToBackStack("ImagePreviewFragment")
            .commit()
    }
}
